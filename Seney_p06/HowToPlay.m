//
//  HowToPlay.m
//  Seney_p06
//
//  Created by Nicholas Ryan Seney on 4/16/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import "HowToPlay.h"
#import "GameScene.h"

@implementation HowToPlay
{
    SKSpriteNode* _backgroundImage;
    SKSpriteNode* _backButton;
}

- (void)didMoveToView:(SKView *)view {
    
    _backgroundImage = [SKSpriteNode spriteNodeWithImageNamed:@"Directions.png"];
    _backgroundImage.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMidY(self.frame));
    _backgroundImage.name = @"BACKGROUND";
    [self addChild:_backgroundImage];
    
    _backButton = [SKSpriteNode spriteNodeWithImageNamed:@"BackButton.png"];
    _backButton.name = @"backButtonNode";
    
    
    _backButton.position = CGPointMake(375, 50);
    [self addChild:_backButton];
    
    
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    if ([node.name isEqualToString:@"backButtonNode"]) {
        NSLog(@"back button pressed");
        SKScene* gameScene = [[GameScene alloc] initWithSize:self.size];
        SKTransition* transition = [SKTransition fadeWithDuration:(.5)];
        [self.view presentScene:gameScene transition:transition];
    }

}


@end
