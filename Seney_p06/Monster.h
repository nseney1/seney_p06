//
//  Monster.h
//  Seney_p06
//
//  Created by Nicholas Ryan Seney on 4/16/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Monster : SKSpriteNode
@property double health,money,moneyModifier,damage;

@end
