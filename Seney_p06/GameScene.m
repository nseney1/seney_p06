//
//  GameScene.m
//  Seney_p06
//
//  Created by Nicholas Ryan Seney on 4/6/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import "GameScene.h"
#import "FirstGameScene.h"
#import "HowToPlay.h"

@implementation GameScene {
    SKSpriteNode* _playButton;
    SKSpriteNode* _howToPlayButton;
    SKSpriteNode* _title;
    SKSpriteNode* _backgroundImage;
    
    
}

- (void)didMoveToView:(SKView *)view {
    // Setup your scene here
    NSLog(@"In GameScene.m");
    
    _backgroundImage = [SKSpriteNode spriteNodeWithImageNamed:@"SpaceLevel.png"];
    _backgroundImage.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMidY(self.frame));
    _backgroundImage.name = @"BACKGROUND";
    [self addChild:_backgroundImage];
    
    _playButton = [SKSpriteNode spriteNodeWithImageNamed:@"PlayButton1.png"];
    
    _playButton.name = @"playButtonNode";
    _playButton.zPosition = 1.0;
    [self addChild:_playButton];
    
    
    _howToPlayButton = [SKSpriteNode spriteNodeWithImageNamed:@"howToPlayButton.png"];
    _howToPlayButton.name = @"howToPlayButtonNode";
    
    
    _howToPlayButton.position = CGPointMake(0.0, -60.0);
    [self addChild:_howToPlayButton];
    
    _title = [SKSpriteNode spriteNodeWithImageNamed:@"SensTapAdventure.png"];
    _title.name = @"SensTapAdventure";
    _title.position = CGPointMake(0.0, 400);
    [self addChild:_title];
    
    
    // Get label node from scene and store it for use later
    
}


- (void)touchDownAtPoint:(CGPoint)pos {
    
}

- (void)touchMovedToPoint:(CGPoint)pos {
    
}

- (void)touchUpAtPoint:(CGPoint)pos {
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    //if fire button touched, bring the rain
    if ([node.name isEqualToString:@"playButtonNode"]) {
        NSLog(@"play button pressed");
        SKScene* firstGameScene = [[FirstGameScene alloc] initWithSize:self.size];
        SKTransition* transition = [SKTransition fadeWithDuration:(.5)];
        [self.view presentScene:firstGameScene transition:transition];
    }
    else if([node.name isEqualToString:@"howToPlayButtonNode"])
    {
        SKScene* howToPlayScene = [[HowToPlay alloc] initWithSize:self.size];
        SKTransition* transition = [SKTransition fadeWithDuration:(.5)];
        [self.view presentScene:howToPlayScene transition:transition];
    }
    
    
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
}
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
}


-(void)update:(CFTimeInterval)currentTime {
    // Called before each frame is rendered
}

@end
