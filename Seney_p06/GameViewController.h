//
//  GameViewController.h
//  Seney_p06
//
//  Created by Nicholas Ryan Seney on 4/6/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import <GameplayKit/GameplayKit.h>
#import "Player.h"


@interface GameViewController : UIViewController
    @property Player* player;
@end


