//
//  FourthGameScene.m
//  Seney_p06
//
//  Created by Nicholas Ryan Seney on 4/16/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import "FourthGameScene.h"
#import "Monster.h"
#import "Player.h"

@implementation FourthGameScene
{
    
    SKSpriteNode* _backgroundImage;
    SKSpriteNode* _upgrades;
    SKTexture* _texture1;
    SKTexture* _texture2;
    SKAction* takeDmg;
    Monster* _monster;
    SKLabelNode* money;
    SKLabelNode* monstHealth;
    SKLabelNode* cost;
    Player* player;
    int moneyModifier;
    int counter;
    
    
}

-(void)didMoveToView:(SKView *)view
{
    counter = 8;
    moneyModifier = 1;
    
    player = [Player alloc];
    _monster = [Monster alloc];
    
    player.damage = 10;
    _backgroundImage = [SKSpriteNode spriteNodeWithImageNamed:@"SpaceLevel.png"];
    _backgroundImage.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMidY(self.frame));
    _backgroundImage.name = @"BACKGROUND";
    [self addChild:_backgroundImage];
    
    _upgrades = [SKSpriteNode spriteNodeWithImageNamed:@"UpgradeButton.png"];
    _upgrades.position = CGPointMake(375,50);
    _upgrades.name = @"upgradeButton";
    [self addChild:_upgrades];
    
    
    
    
    _texture1 = [SKTexture textureWithImageNamed:@"GreenSlime.png"];
    _texture2 = [SKTexture textureWithImageNamed:@"GreenSlime1.png"];
    _monster = [Monster spriteNodeWithTexture:_texture1];
    _monster.position = CGPointMake(self.frame.size.width*.5,self.frame.size.height*.5);
    _monster.health = 200*counter;
    _monster.money = 50;
    
    
    
    
    _monster.name = @"monster";
    [self addChild:_monster];
    money = [[SKLabelNode alloc] init];
    money.text = [NSString stringWithFormat:@"Money: %.02f",player.money];
    money.fontColor = [UIColor whiteColor];
    money.position = CGPointMake(375,100);
    [self addChild:money];
    
    monstHealth = [[SKLabelNode alloc] init];
    monstHealth.text = [NSString stringWithFormat:@"Health: %.02f",_monster.health];
    monstHealth.fontColor = [UIColor whiteColor];
    monstHealth.position = CGPointMake(self.frame.size.width*.5,self.frame.size.height*.5+150);
    [self addChild:monstHealth];
    
    cost = [[SKLabelNode alloc] init];
    cost.text = [NSString stringWithFormat:@"Cost: %d",(50*moneyModifier)];
    cost.fontColor = [UIColor whiteColor];
    cost.position = CGPointMake(550,45);
    [self addChild:cost];
}



- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    
    
    if ([node.name isEqualToString:@"upgradeButton"]) {
        NSLog(@"I touched upgrade button");
        if(player.money > 50)
        {
            player.damageModifier++;
        }
    }
    else if(_monster.health > 0)
    {
        NSLog(@"hit, Monster Health = %f\tPlayer Damage= %f\tPlayer Money= %f",_monster.health,player.damage
              ,player.money);
        takeDmg = [SKAction animateWithTextures:@[_texture2,_texture1] timePerFrame:.1];
        [_monster runAction:takeDmg];
        _monster.health -= (player.damage * (player.damageModifier+1));
        if(_monster.health <= 0)
        {
            player.money += _monster.money;
            money.text = [NSString stringWithFormat:@"Money: %.02f",player.money];
            [_monster removeFromParent];
            counter++;
            if(counter == 9)
            {
                _texture1 = [SKTexture textureWithImageNamed:@"YellowSlime.png"];
                _texture2 = [SKTexture textureWithImageNamed:@"YellowSlime1.png"];
                _monster = [Monster spriteNodeWithTexture:_texture1];
                _monster.position = CGPointMake(self.frame.size.width*.5,self.frame.size.height*.5);
                _monster.health = 200 * counter;
                _monster.money = 200 * counter;
                _monster.damage = 100;
                [self addChild:_monster];
            }
            else if(counter == 10)
            {
                _texture1 = [SKTexture textureWithImageNamed:@"RedSlime.png"];
                _texture2 = [SKTexture textureWithImageNamed:@"RedSlime1.png"];
                _monster = [Monster spriteNodeWithTexture:_texture1];
                _monster.position = CGPointMake(self.frame.size.width*.5,self.frame.size.height*.5);
                _monster.health = 200 * counter;
                _monster.money = 200 * counter;
                _monster.damage = 100;
                [self addChild:_monster];
            }
            else if(counter == 11)
            {
                _texture1 = [SKTexture textureWithImageNamed:@"PurpleSlime.png"];
                _texture2 = [SKTexture textureWithImageNamed:@"PurpleSlime1.png"];
                _monster = [Monster spriteNodeWithTexture:_texture1];
                _monster.position = CGPointMake(self.frame.size.width*.5,self.frame.size.height*.5);
                _monster.health = 200 * counter;
                _monster.money = 200 * counter;
                _monster.damage = 100;
                [self addChild:_monster];
            }
            else if(counter == 12)
            {
                _texture1 = [SKTexture textureWithImageNamed:@"BlackSlime.png"];
                _texture2 = [SKTexture textureWithImageNamed:@"BlackSlime1.png"];
                _monster = [Monster spriteNodeWithTexture:_texture1];
                _monster.position = CGPointMake(self.frame.size.width*.5,self.frame.size.height*.5);
                _monster.health = 200 * counter * counter;
                _monster.money = 200 * counter;
                _monster.damage = 100;
                [self addChild:_monster];
            }
            
            monstHealth.text = [NSString stringWithFormat:@"Health: %.02f",0.00];
        }
        
        
        monstHealth.text = [NSString stringWithFormat:@"Health: %.02f",_monster.health];
        
    }
    
    //if fire button touched, bring the rain
    if ([node.name isEqualToString:@"upgradeButton"]) {
        if(player.money >= (50*(moneyModifier*moneyModifier)))
        {
            player.money -= (50*(moneyModifier*moneyModifier));
            moneyModifier++;
            player.damageModifier++;
            money.text = [NSString stringWithFormat:@"Money: %.02f",player.money];
            cost.text = [NSString stringWithFormat:@"Cost: %d",(50*(moneyModifier*moneyModifier))];
        }
    }
    /*NSLog(@"play button pressed");
     SKScene* firstGameScene = [[UpgradeGameScene alloc] initWithSize:self.size];
     SKTransition* transition = [SKTransition fadeWithDuration:(.5)];
     [self.view presentScene:firstGameScene transition:transition];*/
    //}
    
    
    
    
    
}

@end
