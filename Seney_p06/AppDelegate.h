//
//  AppDelegate.h
//  Seney_p06
//
//  Created by Nicholas Ryan Seney on 4/6/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

